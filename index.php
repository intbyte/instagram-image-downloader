<?php
/**
 * 
 *
 * @author Jlempar
 * @website http://www.jlempar.com
 * @version v1.1
 * @copyright Organisation, default
 * @package default
 **/

/**
 * This is a free tool for
 * Downloader images from instagram
 * And you can not to sell this tool because
 * We share this tool as free
 **/

ini_set('display_errors',0); //error handle

//create function curl to handle images url from instagram
function curl($url, $post = null) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	if($post != null) {
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$exec = curl_exec($ch);
		curl_close($ch);
		return $exec;
		}

if(isset($_POST['submitted'])) {
	
	//default configuration
	$url = strip_tags(filter_var($_POST['url'],FILTER_SANITIZE_URL));
	$errors = array();
	
	//validation
	if(empty($url)) {
		$errors[] = '<font color="red">Please insert a URL from instagram images!</font>';
	} elseif(!filter_var($url,FILTER_VALIDATE_URL)) {
		$errors[] = '<font color="red">Invalid URL!</font>';
	} elseif(empty($errors)) {
		
		//result using json from API INSTAGRAM
		$fetch = curl('https://api.instagram.com/oembed/?url='.$url.'');
		$json = json_decode($fetch);
		
		//display image and for download image user can doubleclick for save
		echo '<center>
		      <a href="'.$json->thumbnail_url.'"><img src="'.$json->thumbnail_url.'" width="250" height="250"/></a>
		      <br/>
		      <b>Click For Download Image</b>
		      </center>';
	}
} else {
	
	//welcome
	echo 'Free Tool For Download A Images From Instagram';
}

//errors handle
foreach($errors as $error) {
	echo $error;
}

//form input url
echo '<form method="POST">
      <b>URL :</b>
      <br/>
      <input name="url" type="text" value="'.$url.'"/>
      <br/>
      <input name="submitted" type="submit" value="DOWNLOAD"/>
      </form>';
?>
